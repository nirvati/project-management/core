#!/bin/bash
set -e
set -o pipefail

echo " ---> PREINSTALL"

echo " ---> Setting up common dependencies. This will take a while..."
./docker/prod/setup/preinstall-common.sh

if test -f ./docker/prod/setup/preinstall-$PLATFORM.sh ; then
	echo " ---> Executing preinstall for $PLATFORM..."
	./docker/prod/setup/preinstall-$PLATFORM.sh
fi

apt-get clean
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
truncate -s 0 /var/log/*log

rm -f /tmp/dockerize.log
echo "      OK."
