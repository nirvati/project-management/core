Gem::Specification.new do |s|
  s.name        = "nepenthes-prometheus"
  s.version     = '0.0.1'

  s.authors     = "The Nepenthes developers"
  s.email       = "nepenthes@nirvati.org"
  s.homepage    = "https://nepenthes.nirvati.org"
  s.summary     = 'Nepenthes Prometheus'
  s.description = "Integrates Prometheus monitoring into Nepenthes"
  s.license     = "GPLv3"

  s.files = Dir["{app,config,db,lib}/**/*"] + %w(CHANGELOG.md README.md)

  s.add_dependency "prometheus_exporter", "~> 2.0"

  s.metadata['rubygems_mfa_required'] = 'true'
end
