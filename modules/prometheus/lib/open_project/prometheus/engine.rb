# Prevent load-order problems in case openproject-plugins is listed after a plugin in the Gemfile
# or not at all
require 'open_project/plugins'

module OpenProject::Prometheus
  class Engine < ::Rails::Engine
    engine_name :nepenthes_prometheus

    include OpenProject::Plugins::ActsAsOpEngine

    register 'nepenthes-prometheus',
             :author_url => 'https://nepenthes.nirvati.org',
             :requires_openproject => '>= 6.0.0',
             bundled: true

  end
end
