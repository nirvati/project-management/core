Gem::Specification.new do |s|
  s.name        = "openproject-recaptcha"
  s.version     = '1.0.0'
  s.authors     = "The Nepenthes developers"
  s.email       = "nepenthes@nirvati.org"
  s.summary     = "Nepenthes ReCaptcha"
  s.description = "This module provides recaptcha checks during login"

  s.files = Dir["{app,config,db,lib}/**/*", "CHANGELOG.md", "README.rdoc"]

  s.add_dependency 'recaptcha', '~> 5.7'
  s.metadata['rubygems_mfa_required'] = 'true'
end
