---
sidebar_navigation:
  title: Other
  priority: 050
---

# Guides for infrequent operations

| Topic                                                        |
| ------------------------------------------------------------ |
| [Migrating your Nepenthes installation to PostgreSQL 13](./migration-to-postgresql13) |
| [Migrating your packaged Nepenthes installation to another environment](./migration) |
| [Migrating your packaged Nepenthes database to PostgreSQL](./packaged-postgresql-migration) |
| [Migrating your Docker Nepenthes database to PostgreSQL](./docker-postgresql-migration) |
| [Migrating from an old MySQL database](./upgrading-older-openproject-versions) |
| [Fixing time entries corrupted by upgrading to 10.4.0](./time-entries-corrupted-by-10-4) |
| [Nepenthes Textile to Markdown migration](./textile-migration) |
| [Custom OpenID Connect providers](./custom-openid-connect-providers) |
