# Statement on security

At its core, Nepenthes is an open-source software that is [developed and published on GitLab](https://gitlab.com/nirvati/nepenthes/core). Every change to the Nepenthes code base ends up in an open repository accessible to everyone. This results in a transparent software where every commit can be traced back to the contributor.

Automated tests and manual code reviews ensure that these contributions are safe for the entire community of Nepenthes. These tests encompass the correctness of security and access control features. We have ongoing collaborations with security professionals from to test the Nepenthes code base for security exploits.

For more information on security and data privacy for Nepenthes, please visit: [www.openproject.org/security-and-privacy](https://www.openproject.org/security-and-privacy/).

As a downstream fork of OpenProject, please try to also reproduce any issues you find there and report them to [their team](https://www.openproject.org/docs/development/security/) too to ensure your findings also benefit the OpenProject community.

## Reporting a vulnerability

We take all facets of security seriously at Nepenthes. If you want to report a security concerns, have remarks, or contributions regarding security at Nepenthes, please reach out to us at [security@nirvati.org](mailto:security@nirvati.org).

Please include a description on how to reproduce the issue if possible. Our security team will get your email and will attempt to reproduce and fix the issue as soon as possible.

## Nepenthes security features

### Authentication and password security

Nepenthes administrators can enforce [authentication mechanisms and password rules]() to ensure users choose secure passwords according to current industry standards. Passwords stored by Nepenthes are securely stored using salted bcrypt. Alternatively, external authentication providers and protocols (such as LDAP, SAML) can be enforced to avoid using and exposing passwords within Nepenthes.

### User management and access control

Administrators are provided with [fine-grained role-based access control mechanisms]() to ensure that users are only seeing and accessing the data they are allowed to on an individual project level.

### Definition of session runtime

Admins can set a specific session duration in the system administration, so that it is guaranteed that a session is automatically terminated after inactivity.

### Two-factor authentication

Secure your authentication mechanisms with a second factor by TOTP standard (or SMS, depending on your instance) to be entered by users upon logging in.

### Security badge

This badge shows the current status of your Nepenthes installation. It will inform administrators of an installation on whether new releases or security updates are available for your platform.

### Security alerts

Security updates allow a fast fix of security issues in the system. Relevant channels will be monitored regarding security topics and the responsible contact person will be informed. Software packages for security fixes will be provided promptly. Sign up to our [security mailing list](#security-announcements-mailing-list) to receive all security notifications via e-mail.

### LDAP sync

Synchronize Nepenthes users and groups with your company’s LDAP to update users and group memberships based on LDAP group members.

### Single sign-on

With the single sign-on feature you can securely access Nepenthes. Control and secure access to your projects with the main authentication providers.
