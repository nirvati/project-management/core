import { defineConfig } from "vitepress";

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "Nepenthes",
  description: "A completely free and open source project management suite.",
  // TODO: Fix dead links
  ignoreDeadLinks: true,
  outDir: "../public",
  themeConfig: {
    editLink: {
      pattern: 'https://gitlab.com/nirvati/nepenthes/core/-/edit/dev/docs/:path'
    },
    search: {
      provider: "local",
    },
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      {
        text: "Getting started",
        link: "/getting-started/",
      },
      {
        text: "User guide",
        link: "/user-guide/",
      },
      {
        text: "BIM guide",
        link: "/bim-guide/",
      },
      {
        text: "Administration",
        items: [
          {
            text: "Installation & operations guide",
            link: "/installation-and-operations/",
          },
          {
            text: "System admin guide",
            link: "/system-admin-guide/",
          },
        ],
      },
      {
        text: "About",
        items: [
          {
            text: "Use Cases",
            link: "/use-cases/",
          },
          {
            text: "FAQ",
            link: "/faq/",
          },
          {
            text: "Glossary",
            link: "/glossary/",
          },
          {
            text: "Release notes",
            link: "/release-notes/",
          },
        ],
      },
      {
        text: "Developers",
        items: [
          {
            text: "Contributing",
            link: "/development/",
          },
          {
            text: "API documentation",
            link: "/api/",
          },
        ],
      },
    ],

    sidebar: {
      "/getting-started/": [
        {
          text: "Getting started",
          link: "/getting-started/",
        },
        {
          text: "Introduction to Nepenthes",
          link: "/getting-started/nepenthes-introduction/",
        },
        {
          text: "Sign in and registration",
          link: "/getting-started/sign-in-registration/",
        },
        {
          text: "Projects",
          link: "/getting-started/projects/",
        },
        {
          text: "Invite members",
          link: "/getting-started/invite-members/",
        },
        {
          text: "Work packages introduction",
          link: "/getting-started/work-packages-introduction/",
        },
        {
          text: "Gantt charts introduction",
          link: "/getting-started/gantt-chart-introduction/",
        },
        {
          text: "Agile boards introduction",
          link: "/getting-started/boards-introduction/",
        },
        {
          text: "My account",
          link: "/getting-started/my-account/",
        },
        {
          text: "My Page",
          link: "/getting-started/my-page/",
        },
        {
          text: "My Activity",
          link: "/getting-started/my-activity/",
        },
      ],
      "/user-guide/": [
        {
          text: "User guide",
          link: "/user-guide/",
        },
        {
          text: "Home page",
          link: "/user-guide/home/",
          items: [
            {
              text: "Global modules",
              link: "/user-guide/home/global-modules/",
            },
          ],
        },
        {
          text: "Project overview",
          link: "/user-guide/project-overview/",
          items: [
            {
              text: "Project overview FAQ",
              link: "/user-guide/project-overview/project-overview-faq/",
            },
          ],
        },
        {
          text: "Project Activity",
          link: "/user-guide/activity/",
        },
        {
          text: "Roadmap",
          link: "/user-guide/roadmap/",
        },
        {
          text: "Work packages",
          link: "/user-guide/work-packages/",
          collapsed: true,
          items: [
            {
              text: "Work packages views",
              link: "/user-guide/work-packages/work-package-views/",
            },
            {
              text: "Create work packages",
              link: "/user-guide/work-packages/create-work-package/",
            },
            {
              text: "Edit work packages",
              link: "/user-guide/work-packages/edit-work-package/",
            },
            {
              text: "Set and change dates and duration",
              link: "/user-guide/work-packages/set-change-dates/",
            },
            {
              text: "Baseline comparison",
              link: "/user-guide/work-packages/baseline-comparison/",
            },
            {
              text: "Copy, move, delete work packages",
              link: "/user-guide/work-packages/copy-move-delete/",
            },
            {
              text: "Configure work package table",
              link: "/user-guide/work-packages/work-package-table-configuration/",
            },
            {
              text: "Work package relations and hierarchies",
              link: "/user-guide/work-packages/work-package-relations-hierarchies/",
            },
            {
              text: "Export work packages",
              link: "/user-guide/work-packages/exporting/",
            },
            {
              text: "Work packages FAQ",
              link: "/user-guide/work-packages/work-packages-faq/",
            },
          ],
        },
        {
          text: "Gantt charts",
          link: "/user-guide/gantt-chart/",
          collapsed: true,
          items: [
            {
              text: "Automatic and manual scheduling",
              link: "/user-guide/gantt-chart/scheduling/",
            },
            {
              text: "Gantt chart FAQ",
              link: "/user-guide/gantt-chart/gantt-chart-faq/",
            },
          ],
        },
        {
          text: "Team planner",
          link: "/user-guide/team-planner/",
        },
        {
          text: "Agile boards",
          link: "/user-guide/agile-boards/",
          items: [
            {
              text: "Agile boards FAQ",
              link: "/user-guide/agile-boards/agile-boards-faq/",
            },
          ],
        },
        {
          text: "Backlogs (Scrum)",
          link: "/user-guide/backlogs-scrum/",
          collapsed: true,
          items: [
            {
              text: "Working with backlogs",
              link: "/user-guide/backlogs-scrum/work-with-backlogs/",
            },
            {
              text: "Manage sprints",
              link: "/user-guide/backlogs-scrum/manage-sprints/",
            },
            {
              text: "Task board view",
              link: "/user-guide/backlogs-scrum/taskboard/",
            },
            {
              text: "Backlogs FAQ",
              link: "/user-guide/backlogs-scrum/backlogs-faq/",
            },
          ],
        },
        {
          text: "Calendar",
          link: "/user-guide/calendar/",
          items: [
            {
              text: "Calendar FAQ",
              link: "/user-guide/calendar/calendar-faq/",
            },
          ],
        },
        {
          text: "News",
          link: "/user-guide/news/",
        },
        {
          text: "Forums",
          link: "/user-guide/forums/",
        },
        {
          text: "Wiki",
          link: "/user-guide/wiki/",
          collapsed: true,
          items: [
            {
              text: "Create and edit wiki",
              link: "/user-guide/wiki/create-edit-wiki/",
            },
            {
              text: "Wiki project menu",
              link: "/user-guide/wiki/wiki-menu/",
            },
            {
              text: "More wiki functions",
              link: "/user-guide/wiki/more-wiki-functions/",
            },
            {
              text: "Wiki FAQ",
              link: "/user-guide/wiki/wiki-faq/",
            },
          ],
        },
        {
          text: "Time and costs",
          link: "/user-guide/time-and-costs/",
          collapsed: true,
          items: [
            {
              text: "Progress tracking",
              link: "/user-guide/time-and-costs/progress-tracking/",
            },
            {
              text: "Time tracking",
              link: "/user-guide/time-and-costs/time-tracking/",
              items: [
                {
                  text: "Toggl integration",
                  link: "/user-guide/time-and-costs/time-tracking/toggl-integration/",
                },
                {
                  text: "TimeCamp integration",
                  link: "/user-guide/time-and-costs/time-tracking/timecamp-integration/",
                },
                {
                  text: "Time Tracker integration",
                  link: "/user-guide/time-and-costs/time-tracking/time-tracker-integration/",
                },
              ],
            },
            {
              text: "Cost tracking",
              link: "/user-guide/time-and-costs/cost-tracking/",
            },
            {
              text: "Time and cost reporting",
              link: "/user-guide/time-and-costs/reporting/",
            },
            {
              text: "Time and costs FAQ",
              link: "/user-guide/time-and-costs/time-and-costs-faq/",
            },
          ],
        },
        {
          text: "Budgets",
          link: "/user-guide/budgets/",
        },
        {
          text: "Documents",
          link: "/user-guide/documents/",
        },
        {
          text: "Meetings",
          link: "/user-guide/meetings/",
        },
        {
          text: "Repository",
          link: "/user-guide/repository/",
        },
        {
          text: "Members",
          link: "/user-guide/members/"
        },
        {
          text: "Projects",
          link: "/user-guide/projects/",
          collapsed: true,
          items: [
            {
              text: "Project settings",
              link: "/user-guide/projects/project-settings/",
              items: [
                {
                  text: "Project information",
                  link: "/user-guide/projects/project-settings/project-information/",
                },
                {
                  text: "Modules",
                  link: "/user-guide/projects/project-settings/modules/",
                },
                {
                  text: "Work package types",
                  link: "/user-guide/projects/project-settings/work-package-types/",
                },
                {
                  text: "Enable work package custom fields",
                  link: "/user-guide/projects/project-settings/custom-fields/",
                },
                {
                  text: "Versions",
                  link: "/user-guide/projects/project-settings/versions/",
                },
                {
                  text: "Work package categories",
                  link: "/user-guide/projects/project-settings/work-package-categories/",
                },
                {
                  text: "Repository",
                  link: "/user-guide/projects/project-settings/repository/",
                },
                {
                  text: "Time tracking activities",
                  link: "/user-guide/projects/project-settings/activities-time-tracking/",
                },
                {
                  text: "Backlogs settings",
                  link: "/user-guide/projects/project-settings/backlogs-settings/",
                },
                {
                  text: "Required disk storage",
                  link: "/user-guide/projects/project-settings/required-disk-storage/",
                },
                {
                  text: "File storages",
                  link: "/user-guide/projects/project-settings/file-storages/",
                },
              ],
            },
            {
              text: "Project status",
              link: "/user-guide/projects/project-status/",
            },
            {
              text: "Project templates",
              link: "/user-guide/projects/project-templates/",
            },
            {
              text: "Projects FAQ",
              link: "/user-guide/projects/projects-faq/",
            },
          ],
        },
        {
          text: "Nextcloud integration",
          link: "/user-guide/nextcloud-integration/",
        },
        {
          text: "Notifications",
          link: "/user-guide/notifications/",
          items: [
            {
              text: "Notifications settings",
              link: "/user-guide/notifications/notification-settings/",
            },
          ],
        },
        {
          text: "Keyboard Shortcuts and Access Keys",
          link: "/user-guide/keyboard-shortcuts-access-keys/",
        },
        {
          text: "Search",
          link: "/user-guide/search/",
        },
        {
          text: "Rich text editor",
          link: "/user-guide/wysiwyg/",
        },
      ],
      "/bim-guide/": [
        {
          text: "IFC viewer",
          link: "/bim-guide/ifc-viewer/",
        },
        {
          text: "BIM issue management",
          link: "/bim-guide/bim-issue-management/",
        },
        {
          text: "Revit add-in",
          link: "/bim-guide/revit-add-in/",
        },
      ],
      "/system-admin-guide/": [
        {
          text: "Initial setup",
          link: "/system-admin-guide/initial-setup/",
        },
        {
          text: "Users and permissions",
          link: "/system-admin-guide/users-permissions/",
          items: [
            {
              text: "User settings",
              link: "/system-admin-guide/users-permissions/settings/",
            },
            {
              text: "Manage users",
              link: "/system-admin-guide/users-permissions/users/",
            },
            {
              text: "Placeholder users",
              link: "/system-admin-guide/users-permissions/placeholder-users/",
            },
            {
              text: "Groups",
              link: "/system-admin-guide/users-permissions/groups/",
            },
            {
              text: "Roles and permissions",
              link: "/system-admin-guide/users-permissions/roles-permissions/",
            },
            {
              text: "Avatars",
              link: "/system-admin-guide/users-permissions/avatars/",
            },
            {
              text: "Users and permissions FAQ",
              link: "/system-admin-guide/users-permissions/users-permissions-faq/",
            },
          ],
        },
        {
          text: "Work packages",
          link: "/system-admin-guide/manage-work-packages/",
          items: [
            {
              text: "Settings",
              link: "/system-admin-guide/manage-work-packages/work-package-settings/",
            },
            {
              text: "Types",
              link: "/system-admin-guide/manage-work-packages/work-package-types/",
            },
            {
              text: "Status",
              link: "/system-admin-guide/manage-work-packages/work-package-status/",
            },
            {
              text: "Workflows",
              link: "/system-admin-guide/manage-work-packages/work-package-workflows/",
            },
            {
              text: "Custom actions",
              link: "/system-admin-guide/manage-work-packages/custom-actions/",
            },
            {
              text: "Work packages FAQ",
              link: "/system-admin-guide/manage-work-packages/work-packages-faq/",
            },
          ],
        },
        {
          text: "Custom fields",
          link: "/system-admin-guide/custom-fields/",
          items: [
            {
              text: "Custom fields for projects",
              link: "/system-admin-guide/custom-fields/custom-fields-projects/",
            },
            {
              text: "Custom fields FAQ",
              link: "/system-admin-guide/custom-fields/custom-fields-faq/",
            },
          ],
        },
        {
          text: "Attribute help texts",
          link: "/system-admin-guide/attribute-help-texts/",
        },
        {
          text: "Enumerations",
          link: "/system-admin-guide/enumerations/",
        },
        {
          text: "Calendar and dates",
          link: "/system-admin-guide/calendars-and-dates/",
        },
        {
          text: "System settings",
          link: "/system-admin-guide/system-settings/",
          items: [
            {
              text: "General settings",
              link: "/system-admin-guide/system-settings/general-settings/",
            },
            {
              text: "Languages",
              link: "/system-admin-guide/system-settings/languages/",
            },
            {
              text: "Project system settings",
              link: "/system-admin-guide/system-settings/project-system-settings/",
            },
            {
              text: "Attachments settings",
              link: "/system-admin-guide/system-settings/attachments/",
            },
            {
              text: "Repositories settings",
              link: "/system-admin-guide/system-settings/repositories/",
            },
          ],
        },
        {
          text: "Emails and notifications",
          link: "/system-admin-guide/emails-and-notifications/",
        },
        {
          text: "API and webhooks",
          link: "/system-admin-guide/api-and-webhooks/",
        },
        {
          text: "Authentication",
          link: "/system-admin-guide/authentication/",
          items: [
            {
              text: "Settings",
              link: "/system-admin-guide/authentication/authentication-settings/",
            },
            {
              text: "OAuth applications",
              link: "/system-admin-guide/authentication/oauth-applications/",
            },
            {
              text: "OpenID providers",
              link: "/system-admin-guide/authentication/openid-providers/",
            },
            {
              text: "SAML single sign-on",
              link: "/system-admin-guide/authentication/saml/",
            },
            {
              text: "Kerberos",
              link: "/system-admin-guide/authentication/kerberos/",
            },
            {
              text: "Two-factor authentication",
              link: "/system-admin-guide/authentication/two-factor-authentication/",
            },
            {
              text: "reCAPTCHA",
              link: "/system-admin-guide/authentication/recaptcha/",
            },
            {
              text: "LDAP authentication",
              link: "/system-admin-guide/authentication/ldap-authentication/",
              items: [
                {
                  text: "LDAP group synchronization",
                  link: "/system-admin-guide/authentication/ldap-authentication/ldap-group-synchronization/",
                },
              ],
            },
            {
              text: "Authentication FAQ",
              link: "/system-admin-guide/authentication/authentication-faq/",
            },
          ],
        },
        {
          text: "Announcement",
          link: "/system-admin-guide/announcement/",
        },
        {
          text: "Design",
          link: "/system-admin-guide/design/",
        },
        {
          text: "Colors",
          link: "/system-admin-guide/colors/",
        },
        {
          text: "Time and costs",
          link: "/system-admin-guide/time-and-costs/",
        },
        {
          text: "Backlogs",
          link: "/system-admin-guide/backlogs/",
        },
        {
          text: "Plugins",
          link: "/system-admin-guide/plugins/",
        },
        {
          text: "Backup",
          link: "/system-admin-guide/backup/",
        },
        {
          text: "Information",
          link: "/system-admin-guide/information/",
        },
        {
          text: "Integrations",
          link: "/system-admin-guide/integrations/",
          items: [
            {
              text: "GitHub integration",
              link: "/system-admin-guide/integrations/github-integration/",
            },
            {
              text: "Nextcloud integration setup",
              link: "/system-admin-guide/integrations/nextcloud/",
            },
            {
              text: "Excel synchronization",
              link: "/system-admin-guide/integrations/excel-synchronization/",
            },
            {
              text: "Integrations FAQ",
              link: "/system-admin-guide/integrations/integrations-faq/",
            },
          ],
        },
        {
          text: "System admin FAQ",
          link: "/system-admin-guide/system-admin-guide-faq/",
        },
      ],
      "/use-cases/": [
        {
          text: "Resource Management",
          link: "/use-cases/resource-management/",
        },
        {
          text: "Portfolio Management and Custom Reporting",
          link: "/use-cases/portfolio-management/",
        },
      ],
      "/installation-and-operations/": [
        {
          text: "System requirements",
          link: "/installation-and-operations/system-requirements/",
        },
        {
          text: "Installation",
          link: "/installation-and-operations/installation/",
          items: [
            {
              text: "Packages",
              link: "/installation-and-operations/installation/packaged/",
            },
            {
              text: "Docker",
              link: "/installation-and-operations/installation/docker/",
            },
            {
              text: "Kubernetes",
              link: "/installation-and-operations/installation/kubernetes/",
            },
            {
              text: "Helm Chart",
              link: "/installation-and-operations/installation/helm-chart/",
            },
            {
              text: "Other",
              link: "/installation-and-operations/installation/misc/",
            },
            {
              text: "Univention App Center",
              link: "/installation-and-operations/installation/univention/",
            },
          ],
        },
        {
          text: "Operation",
          link: "/installation-and-operations/operation/",
          items: [
            {
              text: "(Re)configuring",
              link: "/installation-and-operations/operation/reconfiguring/",
            },
            {
              text: "Backing up",
              link: "/installation-and-operations/operation/backing-up/",
            },
            {
              text: "Restoring",
              link: "/installation-and-operations/operation/restoring/",
            },
            {
              text: "Upgrading",
              link: "/installation-and-operations/operation/upgrading/",
            },
            {
              text: "Monitoring & Logs",
              link: "/installation-and-operations/operation/monitoring/",
            },
            {
              text: "Process control",
              link: "/installation-and-operations/operation/control/",
            },
            {
              text: "FAQ",
              link: "/installation-and-operations/operation/faq/",
            },
          ],
        },
        {
          text: "BIM edition",
          link: "/installation-and-operations/bim-edition/",
        },
        {
          text: "Advanced configuration",
          link: "/installation-and-operations/configuration/",
          items: [
            {
              text: "Environment variables",
              link: "/installation-and-operations/configuration/environment/",
            },
            {
              text: "Configuring SSL",
              link: "/installation-and-operations/configuration/ssl/",
            },
            {
              text: "Configuring outbound emails",
              link: "/installation-and-operations/configuration/outbound-emails/",
            },
            {
              text: "Configuring inbound emails",
              link: "/installation-and-operations/configuration/incoming-emails/",
            },
            {
              text: "Configuring a custom database server",
              link: "/installation-and-operations/configuration/database/",
            },
            {
              text: "Configuring a custom web server",
              link: "/installation-and-operations/configuration/server/",
            },
            {
              text: "Adding plugins",
              link: "/installation-and-operations/configuration/plugins/",
            },
          ],
        },
        {
          text: "Other",
          link: "/installation-and-operations/misc/",
        },
        {
          text: "Installation & Ops FAQ",
          link: "/installation-and-operations/installation-faq/",
        },
      ],
      "/release-notes/": [
        {
          text: "13.0.7",
          link: "/release-notes/13-0-7/",
        },
        {
          text: "13.0.6",
          link: "/release-notes/13-0-6/",
        },
        {
          text: "13.0.5",
          link: "/release-notes/13-0-5/",
        },
        {
          text: "13.0.4",
          link: "/release-notes/13-0-4/",
        },
        {
          text: "13.0.3",
          link: "/release-notes/13-0-3/",
        },
        {
          text: "13.0.2",
          link: "/release-notes/13-0-2/",
        },
        {
          text: "13.0.1",
          link: "/release-notes/13-0-1/",
        },
        {
          text: "13.0.0",
          link: "/release-notes/13-0-0/",
        },
        {
          text: "12.x",
          link: "/release-notes/12/",
          items: [
            {
              text: "12.5.8",
              link: "/release-notes/12/12-5-8/",
            },
            {
              text: "12.5.7",
              link: "/release-notes/12/12-5-7/",
            },
            {
              text: "12.5.6",
              link: "/release-notes/12/12-5-6/",
            },
            {
              text: "12.5.5",
              link: "/release-notes/12/12-5-5/",
            },
            {
              text: "12.5.4",
              link: "/release-notes/12/12-5-4/",
            },
            {
              text: "12.5.3",
              link: "/release-notes/12/12-5-3/",
            },
            {
              text: "12.5.2",
              link: "/release-notes/12/12-5-2/",
            },
            {
              text: "12.5.1",
              link: "/release-notes/12/12-5-1/",
            },
            {
              text: "12.5.0",
              link: "/release-notes/12/12-5-0/",
            },
            {
              text: "12.4.5",
              link: "/release-notes/12/12-4-5/",
            },
            {
              text: "12.4.4",
              link: "/release-notes/12/12-4-4/",
            },
            {
              text: "12.4.3",
              link: "/release-notes/12/12-4-3/",
            },
            {
              text: "12.4.2",
              link: "/release-notes/12/12-4-2/",
            },
            {
              text: "12.4.1",
              link: "/release-notes/12/12-4-1/",
            },
            {
              text: "12.4.0",
              link: "/release-notes/12/12-4-0/",
            },
            {
              text: "12.3.4",
              link: "/release-notes/12/12-3-4/",
            },
            {
              text: "12.3.3",
              link: "/release-notes/12/12-3-3/",
            },
            {
              text: "12.3.2",
              link: "/release-notes/12/12-3-2/",
            },
            {
              text: "12.3.1",
              link: "/release-notes/12/12-3-1/",
            },
            {
              text: "12.3.0",
              link: "/release-notes/12/12-3-0/",
            },
            {
              text: "12.2.5",
              link: "/release-notes/12/12-2-5/",
            },
            {
              text: "12.2.4",
              link: "/release-notes/12/12-2-4/",
            },
            {
              text: "12.2.3",
              link: "/release-notes/12/12-2-3/",
            },
            {
              text: "12.2.2",
              link: "/release-notes/12/12-2-2/",
            },
            {
              text: "12.2.1",
              link: "/release-notes/12/12-2-1/",
            },
            {
              text: "12.2.0",
              link: "/release-notes/12/12-2-0/",
            },
            {
              text: "12.1.6",
              link: "/release-notes/12/12-1-6/",
            },
            {
              text: "12.1.5",
              link: "/release-notes/12/12-1-5/",
            },
            {
              text: "12.1.4",
              link: "/release-notes/12/12-1-4/",
            },
            {
              text: "12.1.3",
              link: "/release-notes/12/12-1-3/",
            },
            {
              text: "12.1.2",
              link: "/release-notes/12/12-1-2/",
            },
            {
              text: "12.1.1",
              link: "/release-notes/12/12-1-1/",
            },
            {
              text: "12.1.0",
              link: "/release-notes/12/12-1-0/",
            },
            {
              text: "12.0.10",
              link: "/release-notes/12/12-0-10/",
            },
            {
              text: "12.0.9",
              link: "/release-notes/12/12-0-9/",
            },
            {
              text: "12.0.8",
              link: "/release-notes/12/12-0-8/",
            },
            {
              text: "12.0.7",
              link: "/release-notes/12/12-0-7/",
            },
            {
              text: "12.0.6",
              link: "/release-notes/12/12-0-6/",
            },
            {
              text: "12.0.5",
              link: "/release-notes/12/12-0-5/",
            },
            {
              text: "12.0.4",
              link: "/release-notes/12/12-0-4/",
            },
            {
              text: "12.0.3",
              link: "/release-notes/12/12-0-3/",
            },
            {
              text: "12.0.2",
              link: "/release-notes/12/12-0-2/",
            },
            {
              text: "12.0.1",
              link: "/release-notes/12/12-0-1/",
            },
            {
              text: "12.0.0",
              link: "/release-notes/12/12-0-0/",
            },
          ],
        },
        {
          text: "11.x",
          link: "/release-notes/11/",
          items: [
            {
              text: "11.4.1",
              link: "/release-notes/11/11-4-1/",
            },
            {
              text: "11.4.0",
              link: "/release-notes/11/11-4-0/",
            },
            {
              text: "11.3.5",
              link: "/release-notes/11/11-3-5/",
            },
            {
              text: "11.3.4",
              link: "/release-notes/11/11-3-4/",
            },
            {
              text: "11.3.3",
              link: "/release-notes/11/11-3-3/",
            },
            {
              text: "11.3.2",
              link: "/release-notes/11/11-3-2/",
            },
            {
              text: "11.3.1",
              link: "/release-notes/11/11-3-1/",
            },
            {
              text: "11.3.0",
              link: "/release-notes/11/11-3-0/",
            },
            {
              text: "11.2.4",
              link: "/release-notes/11/11-2-4/",
            },
            {
              text: "11.2.3",
              link: "/release-notes/11/11-2-3/",
            },
            {
              text: "11.2.2",
              link: "/release-notes/11/11-2-2/",
            },
            {
              text: "11.2.1",
              link: "/release-notes/11/11-2-1/",
            },
            {
              text: "11.2.0",
              link: "/release-notes/11/11-2-0/",
            },
            {
              text: "11.1.4",
              link: "/release-notes/11/11-1-4/",
            },
            {
              text: "11.1.3",
              link: "/release-notes/11/11-1-3/",
            },
            {
              text: "11.1.2",
              link: "/release-notes/11/11-1-2/",
            },
            {
              text: "11.1.1",
              link: "/release-notes/11/11-1-1/",
            },
            {
              text: "11.1.0",
              link: "/release-notes/11/11-1-0/",
            },
            {
              text: "11.0.4",
              link: "/release-notes/11/11-0-4/",
            },
            {
              text: "11.0.3",
              link: "/release-notes/11/11-0-3/",
            },
            {
              text: "11.0.2",
              link: "/release-notes/11/11-0-2/",
            },
            {
              text: "11.0.1",
              link: "/release-notes/11/11-0-1/",
            },
            {
              text: "11.0.0",
              link: "/release-notes/11/11-0-0/",
            },
          ],
        },
        {
          text: "10.x",
          link: "/release-notes/10/",
          items: [
            {
              text: "10.6.5",
              link: "/release-notes/10/10-6-5/",
            },
            {
              text: "10.6.4",
              link: "/release-notes/10/10-6-4/",
            },
            {
              text: "10.6.3",
              link: "/release-notes/10/10-6-3/",
            },
            {
              text: "10.6.2",
              link: "/release-notes/10/10-6-2/",
            },
            {
              text: "10.6.1",
              link: "/release-notes/10/10-6-1/",
            },
            {
              text: "10.6.0",
              link: "/release-notes/10/10-6-0/",
            },
            {
              text: "10.5.1",
              link: "/release-notes/10/10-5-1/",
            },
            {
              text: "10.5.0",
              link: "/release-notes/10/10-5-0/",
            },
            {
              text: "10.4.1",
              link: "/release-notes/10/10-4-1/",
            },
            {
              text: "10.4.0",
              link: "/release-notes/10/10-4-0/",
            },
            {
              text: "10.3.1",
              link: "/release-notes/10/10-3-1/",
            },
            {
              text: "10.3.0",
              link: "/release-notes/10/10-3-0/",
            },
            {
              text: "10.2.2",
              link: "/release-notes/10/10-2-2/",
            },
            {
              text: "10.2.1",
              link: "/release-notes/10/10-2-1/",
            },
            {
              text: "10.2.0",
              link: "/release-notes/10/10-2-0/",
            },
            {
              text: "10.1.0",
              link: "/release-notes/10/10-1-0/",
            },
            {
              text: "10.0.2",
              link: "/release-notes/10/10-0-2/",
            },
            {
              text: "10.0.1",
              link: "/release-notes/10/10-0-1/",
            },
            {
              text: "10.0.0",
              link: "/release-notes/10/10-0-0/",
            },
          ],
        },
        {
          text: "9.x",
          link: "/release-notes/9/",
          items: [
            {
              text: "9.0.4",
              link: "/release-notes/9/9-0-4/",
            },
            {
              text: "9.0.3",
              link: "/release-notes/9/9-0-3/",
            },
            {
              text: "9.0.2",
              link: "/release-notes/9/9-0-2/",
            },
            {
              text: "9.0.1",
              link: "/release-notes/9/9-0-1/",
            },
            {
              text: "9.0.0",
              link: "/release-notes/9/9-0-0/",
            },
          ],
        },
        {
          text: "8.x",
          link: "/release-notes/8/",
          items: [
            {
              text: "8.3.2",
              link: "/release-notes/8/8-3-2/",
            },
            {
              text: "8.3.1",
              link: "/release-notes/8/8-3-1/",
            },
            {
              text: "8.3.0",
              link: "/release-notes/8/8-3-0/",
            },
            {
              text: "8.2.1",
              link: "/release-notes/8/8-2-1/",
            },
            {
              text: "8.2.0",
              link: "/release-notes/8/8-2-0/",
            },
            {
              text: "8.1.0",
              link: "/release-notes/8/8-1-0/",
            },
            {
              text: "8.0.2",
              link: "/release-notes/8/8-0-2/",
            },
            {
              text: "8.0.1",
              link: "/release-notes/8/8-0-1/",
            },
            {
              text: "8.0.0",
              link: "/release-notes/8/8-0-0/",
            },
          ],
        },
        {
          text: "7.x",
          link: "/release-notes/7/",
          items: [
            {
              text: "7.4.7",
              link: "/release-notes/7/7-4-7/",
            },
            {
              text: "7.4.6",
              link: "/release-notes/7/7-4-6/",
            },
            {
              text: "7.4.5",
              link: "/release-notes/7/7-4-5/",
            },
            {
              text: "7.4.4",
              link: "/release-notes/7/7-4-4/",
            },
            {
              text: "7.4.3",
              link: "/release-notes/7/7-4-3/",
            },
            {
              text: "7.4.2",
              link: "/release-notes/7/7-4-2/",
            },
            {
              text: "7.4.1",
              link: "/release-notes/7/7-4-1/",
            },
            {
              text: "7.4.0",
              link: "/release-notes/7/7-4-0/",
            },
            {
              text: "7.3.2",
              link: "/release-notes/7/7-3-2/",
            },
            {
              text: "7.3.1",
              link: "/release-notes/7/7-3-1/",
            },
            {
              text: "7.3.0",
              link: "/release-notes/7/7-3-0/",
            },
            {
              text: "7.2.3",
              link: "/release-notes/7/7-2-3/",
            },
            {
              text: "7.2.2",
              link: "/release-notes/7/7-2-2/",
            },
            {
              text: "7.2.1",
              link: "/release-notes/7/7-2-1/",
            },
            {
              text: "7.2.0",
              link: "/release-notes/7/7-2-0/",
            },
            {
              text: "7.1.0",
              link: "/release-notes/7/7-1-0/",
            },
            {
              text: "7.0.3",
              link: "/release-notes/7/7-0-3/",
            },
            {
              text: "7.0.2",
              link: "/release-notes/7/7-0-2/",
            },
            {
              text: "7.0.1",
              link: "/release-notes/7/7-0-1/",
            },
            {
              text: "7.0.0",
              link: "/release-notes/7/7-0-0/",
            },
          ],
        },
        {
          text: "6.x",
          link: "/release-notes/6/",
          items: [
            {
              text: "6.1.6",
              link: "/release-notes/6/6-1-6/",
            },
            {
              text: "6.1.5",
              link: "/release-notes/6/6-1-5/",
            },
            {
              text: "6.1.4",
              link: "/release-notes/6/6-1-4/",
            },
            {
              text: "6.1.3",
              link: "/release-notes/6/6-1-3/",
            },
            {
              text: "6.1.2",
              link: "/release-notes/6/6-1-2/",
            },
            {
              text: "6.1.1",
              link: "/release-notes/6/6-1-1/",
            },
            {
              text: "6.1.0",
              link: "/release-notes/6/6-1-0/",
            },
            {
              text: "6.0.5",
              link: "/release-notes/6/6-0-5/",
            },
            {
              text: "6.0.4",
              link: "/release-notes/6/6-0-4/",
            },
            {
              text: "6.0.3",
              link: "/release-notes/6/6-0-3/",
            },
            {
              text: "6.0.2",
              link: "/release-notes/6/6-0-2/",
            },
            {
              text: "6.0.1",
              link: "/release-notes/6/6-0-1/",
            },
            {
              text: "6.0.0",
              link: "/release-notes/6/6-0-0/",
            },
          ],
        },
        {
          text: "5.x",
          link: "/release-notes/5/",
          items: [
            {
              text: "5.0.20",
              link: "/release-notes/5/5-0-20/",
            },
            {
              text: "5.0.19",
              link: "/release-notes/5/5-0-19/",
            },
            {
              text: "5.0.18",
              link: "/release-notes/5/5-0-18/",
            },
            {
              text: "5.0.17",
              link: "/release-notes/5/5-0-17/",
            },
            {
              text: "5.0.16",
              link: "/release-notes/5/5-0-16/",
            },
            {
              text: "5.0.15",
              link: "/release-notes/5/5-0-15/",
            },
            {
              text: "5.0.14",
              link: "/release-notes/5/5-0-14/",
            },
            {
              text: "5.0.13",
              link: "/release-notes/5/5-0-13/",
            },
            {
              text: "5.0.12",
              link: "/release-notes/5/5-0-12/",
            },
            {
              text: "5.0.11",
              link: "/release-notes/5/5-0-11/",
            },
            {
              text: "5.0.10",
              link: "/release-notes/5/5-0-10/",
            },
            {
              text: "5.0.9",
              link: "/release-notes/5/5-0-9/",
            },
            {
              text: "5.0.8",
              link: "/release-notes/5/5-0-8/",
            },
            {
              text: "5.0.7",
              link: "/release-notes/5/5-0-7/",
            },
            {
              text: "5.0.6",
              link: "/release-notes/5/5-0-6/",
            },
            {
              text: "5.0.5",
              link: "/release-notes/5/5-0-5/",
            },
            {
              text: "5.0.4",
              link: "/release-notes/5/5-0-4/",
            },
            {
              text: "5.0.3",
              link: "/release-notes/5/5-0-3/",
            },
            {
              text: "5.0.2",
              link: "/release-notes/5/5-0-2/",
            },
            {
              text: "5.0.1",
              link: "/release-notes/5/5-0-1/",
            },
            {
              text: "5.0.0",
              link: "/release-notes/5/5-0-0/",
            },
          ],
        },
        {
          text: "4.x",
          link: "/release-notes/4/",
          items: [
            {
              text: "4.2.9",
              link: "/release-notes/4/4-2-9/",
            },
            {
              text: "4.2.8",
              link: "/release-notes/4/4-2-8/",
            },
            {
              text: "4.2.7",
              link: "/release-notes/4/4-2-7/",
            },
            {
              text: "4.2.6",
              link: "/release-notes/4/4-2-6/",
            },
            {
              text: "4.2.5",
              link: "/release-notes/4/4-2-5/",
            },
            {
              text: "4.2.4",
              link: "/release-notes/4/4-2-4/",
            },
            {
              text: "4.2.3",
              link: "/release-notes/4/4-2-3/",
            },
            {
              text: "4.2.2",
              link: "/release-notes/4/4-2-2/",
            },
            {
              text: "4.2.1",
              link: "/release-notes/4/4-2-1/",
            },
            {
              text: "4.2.0",
              link: "/release-notes/4/4-2-0/",
            },
            {
              text: "4.1.4",
              link: "/release-notes/4/4-1-4/",
            },
            {
              text: "4.1.3",
              link: "/release-notes/4/4-1-3/",
            },
            {
              text: "4.1.2",
              link: "/release-notes/4/4-1-2/",
            },
            {
              text: "4.1.1",
              link: "/release-notes/4/4-1-1/",
            },
            {
              text: "4.1.0",
              link: "/release-notes/4/4-1-0/",
            },
            {
              text: "4.0.12",
              link: "/release-notes/4/4-0-12/",
            },
            {
              text: "4.0.11",
              link: "/release-notes/4/4-0-11/",
            },
            {
              text: "4.0.10",
              link: "/release-notes/4/4-0-10/",
            },
            {
              text: "4.0.9",
              link: "/release-notes/4/4-0-9/",
            },
            {
              text: "4.0.8",
              link: "/release-notes/4/4-0-8/",
            },
            {
              text: "4.0.7",
              link: "/release-notes/4/4-0-7/",
            },
            {
              text: "4.0.6",
              link: "/release-notes/4/4-0-6/",
            },
            {
              text: "4.0.5",
              link: "/release-notes/4/4-0-5/",
            },
            {
              text: "4.0.4",
              link: "/release-notes/4/4-0-4/",
            },
            {
              text: "4.0.3",
              link: "/release-notes/4/4-0-3/",
            },
            {
              text: "4.0.2",
              link: "/release-notes/4/4-0-2/",
            },
            {
              text: "4.0.1",
              link: "/release-notes/4/4-0-1/",
            },
            {
              text: "4.0.0",
              link: "/release-notes/4/4-0-0/",
            },
          ],
        },
        {
          text: "3.x",
          link: "/release-notes/3/",
          items: [
            {
              text: "3.0.17",
              link: "/release-notes/3/3-0-17/",
            },
            {
              text: "3.0.16",
              link: "/release-notes/3/3-0-16/",
            },
            {
              text: "3.0.15",
              link: "/release-notes/3/3-0-15/",
            },
            {
              text: "3.0.14",
              link: "/release-notes/3/3-0-14/",
            },
            {
              text: "3.0.13",
              link: "/release-notes/3/3-0-13/",
            },
            {
              text: "3.0.12",
              link: "/release-notes/3/3-0-12/",
            },
            {
              text: "3.0.11",
              link: "/release-notes/3/3-0-11/",
            },
            {
              text: "3.0.8",
              link: "/release-notes/3/3-0-8/",
            },
            {
              text: "3.0.4",
              link: "/release-notes/3/3-0-4/",
            },
            {
              text: "3.0.3",
              link: "/release-notes/3/3-0-3/",
            },
            {
              text: "3.0.1",
              link: "/release-notes/3/3-0-1/",
            },
            {
              text: "3.0.0",
              link: "/release-notes/3/3-0-0/",
            },
          ],
        },
      ],
      "/development/": [
        {
          text: "Contribute to documentation",
          link: "/development/contribution-documentation/",
          items: [
            {
              text: "Documentation process",
              link: "/development/contribution-documentation/documentation-process/",
            },
            {
              text: "Internal docs contributor",
              link: "/development/contribution-documentation/documentation-process-internal-contributor/",
            },
            {
              text: "Documentation style guide",
              link: "/development/contribution-documentation/documentation-style-guide/",
            },
            {
              text: "Support for contribution",
              link: "/development/contribution-documentation/contribution-support/",
            },
          ],
        },
        {
          text: "Application architecture",
          link: "/development/application-architecture/",
        },
        {
          text: "Product development",
          link: "/development/product-development-handbook/",
        },
        {
          text: "Primer Design system",
          link: "/development/design-system/",
        },
        {
          text: "Report a bug",
          link: "/development/report-a-bug/",
        },
        {
          text: "Submit a feature idea",
          link: "/development/submit-feature-idea/",
        },
        {
          text: "Translate Nepenthes",
          link: "/development/translate-nepenthes/",
        },
        {
          text: "SAML development setup",
          link: "/development/saml/",
        },
        {
          text: "Packaging",
          link: "/development/packaging/",
        },
        {
          text: "Kerberos development setup",
          link: "/development/kerberos/",
        },
        {
          text: "Nepenthes First Look",
          link: "/development/first-look/",
        },
        {
          text: "Development setup via docker",
          link: "/development/development-environment-docker/",
        },
        {
          text: "Development setup on Debian / Ubuntu",
          link: "/development/development-environment-ubuntu/",
        },
        {
          text: "Development setup via docker on MacOS",
          link: "/development/development-environment-docker-macos/",
        },
        {
          text: "Development setup on MacOS",
          link: "/development/development-environment-osx/",
        },
        {
          text: "Development workflow",
          link: "/development/git-workflow/",
        },
        {
          text: "Development concepts",
          link: "/development/concepts/",
          items: [
            {
              text: "Inline editing",
              link: "/development/concepts/inline-editing/",
            },
            {
              text: "Schemas",
              link: "/development/concepts/resource-schemas/",
            },
            {
              text: "Resource changesets",
              link: "/development/concepts/resource-changesets/",
            },
            {
              text: "HAL+JSON resources",
              link: "/development/concepts/hal-resources/",
            },
            {
              text: "Using Stimulus",
              link: "/development/concepts/stimulus/",
            },
            {
              text: "Permissions",
              link: "/development/concepts/permissions/",
            },
            {
              text: "State management",
              link: "/development/concepts/state-management/",
            },
            {
              text: "Dynamically generated forms",
              link: "/development/concepts/dynamic-forms/",
            },
            {
              text: "Queries",
              link: "/development/concepts/queries/",
            },
            {
              text: "Translations",
              link: "/development/concepts/translations/",
            },
          ],
        },
      ],
      "/api/": [
        {
          text: "FAQ",
          link: "/api/faq/",
        },
      ],
    },

    socialLinks: [
      {
        icon: {
          svg: '<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 256 256"><path fill="currentColor" d="M230.15 117.1L210.25 41a11.94 11.94 0 0 0-22.79-1.11L169.78 88H86.22L68.54 39.87A11.94 11.94 0 0 0 45.75 41l-19.9 76.1a57.19 57.19 0 0 0 22 61l73.27 51.76a11.91 11.91 0 0 0 13.74 0l73.27-51.76a57.19 57.19 0 0 0 22.02-61ZM58 57.5l15.13 41.26a8 8 0 0 0 7.51 5.24h94.72a8 8 0 0 0 7.51-5.24L198 57.5l13.07 50L128 166.21L44.9 107.5Zm-17.32 66.61L114.13 176l-20.72 14.65L57.09 165a41.06 41.06 0 0 1-16.41-40.89Zm87.32 91l-20.73-14.65L128 185.8l20.73 14.64ZM198.91 165l-36.32 25.66L141.87 176l73.45-51.9a41.06 41.06 0 0 1-16.41 40.9Z"/></svg>',
        },
        link: "https://gitlab.com/nirvati/nepenthes/core",
      },
    ],
  },
});
