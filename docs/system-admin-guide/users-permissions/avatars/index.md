---
sidebar_navigation:
  title: Avatars
  priority: 960
description: Manage Avatars in Nepenthes
keywords: Avatars
---
# Nepenthes Avatars

To select which type of Avatars can be used in your Nepenthes, navigate to **Administration -> Users and permissions -> Avatars**.

You can choose whether to allow user gravatars or enable to upload custom avatars.

The Avatars can be configured via the [user profile](../users).

![Nepenthes avatars](system-guide-avatar.png)

