# Nepenthes

Nepenthes is a completely free, libre web-based project management software. Its key features are:

* [Project planning and scheduling](https://www.openproject.org/collaboration-software-features/#project-planning)
* [Product roadmap and release planning](https://www.openproject.org/collaboration-software-features/#product-management)
* [Task management and team collaboration](https://www.openproject.org/collaboration-software-features/#task-management)
* [Agile and Scrum](https://www.openproject.org/collaboration-software-features/#agile-scrum)
* [Time tracking, cost reporting, and budgeting](https://www.openproject.org/collaboration-software-features/#time-tracking)
* [Bug tracking](https://www.openproject.org/collaboration-software-features/#bug-tracking)
* [Wikis](https://nepenthes.nirvati.org/user-guide/wysiwyg/)
* [Forums](https://nepenthes.nirvati.org/user-guide/forums/)
* [Meeting agendas and meeting minutes](https://nepenthes.nirvati.org/user-guide/meetings/)

More information and screenshots can be found on our [website](https://www.openproject.org).

## Installation

If you want to run an instance of Nepenthes in production (or for evaluation), refer to our
in-depth [installation guides](https://nepenthes.nirvati.org/installation-and-operations/).

## Reporting bugs

You found a bug? Please [report it](https://nepenthes.nirvati.org/development/report-a-bug/) on our [GitLab repository](https://gitlab.com/nirvati/nepenthes/core/-/issues). Thank you!

## Contribute

Nepenthes is supported by its community members, both companies and individuals.

We are always looking for new members to our community, so if you are interested in improving Nepenthes we would be glad to welcome and support you getting into the code. There are guides as well, e.g. a [Quick Start for Developers](https://nepenthes.nirvati.org/development/#development-environment).

## Security / responsible disclosure

We take security very seriously at Nepenthes. We value any kind of feedback that
will keep our community secure. If you happen to come across a security issue we urge
you to disclose it to us privately to allow our users and community enough time to
upgrade. Security issues will always take precedence over anything else in the pipeline.

For more information on how to disclose a security vulnerability, [please see this page](docs/development/security/README.md).

## License

Nepenthes is licensed under the terms of the GNU General Public License version 3.
See [COPYRIGHT](COPYRIGHT) and [LICENSE](LICENSE) files for details.

## Credits

### OpenProject

Nepenthes is a fork of OpenProject, and would not be possible without the work of the OpenProject community.


### Icons

Thanks to Vincent Le Moign and his fabulous Minicons icons on [webalys.com](http://www.webalys.com/minicons/icons-free-pack.php).

### Lato Font

Thanks to Łukasz Dziedzic (aka "tyPoland") for his 'Lato' font.

### OpenProject icon font
Published and created by the OpenProject Foundation (OPF) under [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/)
with icons from the following sources
[Minicons Free Vector Icons Pack](http://www.webalys.com/minicons) and
[User Interface Design framework](http://www.webalys.com/design-interface-application-framework.php) both by webalys

**Creative Commons License**

OpenProject Icon Font by the OpenProject Foundation (OPF) is licensed under Creative Commons Attribution 3.0 Unported License
and Free for both personal and commercial use. You can copy, adapt, remix, distribute or transmit it.

Under this condition: provide a mention of the "OpenProject Foundation" and a link back to OpenProject www.openproject.org.
